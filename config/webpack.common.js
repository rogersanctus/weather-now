const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const dotenv = require('dotenv')
const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const { EnvironmentPlugin } = require('webpack')

const env = dotenv.config({
  path: path.resolve(__dirname, '../.env')
})

let environmentPluginConfig
let envObj = {}

if (env.error) {
  environmentPluginConfig = new EnvironmentPlugin()
  envObj = {
    'process.env': JSON.stringify(
      Object.keys(process.env)
        .filter((key) => key.startsWith('API_'))
        .reduce((accum, curr) => {
          accum[curr] = process.env[curr]
          return accum
        }, {})
    )
  }
  console.warn('WARN: .env file could not be loaded.')
} else {
  environmentPluginConfig = new EnvironmentPlugin(Object.keys(env.parsed))
}

module.exports = {
  entry: path.resolve(__dirname, '../src/index.js'),
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '/build'),
    clean: true
  },
  resolve: {
    extensions: ['.json', '.js', '.vue', '...'],
    alias: {
      '@': path.resolve(__dirname, '../src'),
      '@styles': path.resolve(__dirname, '../src/assets/styles')
    }
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../src/index.html'),
      filename: 'index.html',
      title: 'WeatherNow',
      favicon: './public/favicon_32x32.png'
    }),
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: 'true',
      __VUE_PROD_DEVTOOLS__: 'false',
      ...envObj
    }),
    environmentPluginConfig
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        exclude: [
          /node_modules/,
          /core-js/,
          /webpack\/buildin/,
          /@babel\/runtime-corejs3/
        ],
        use: ['thread-loader', 'babel-loader']
      },
      // images
      {
        test: /\.(png|jpe?g|gif|webp)(\?.*)?$/,
        type: 'asset',
        generator: { filename: 'img/[contenthash:8][ext][query]' }
      },

      // do not base64-inline SVGs.
      // https://github.com/facebookincubator/create-react-app/pull/1180
      {
        test: /\.(svg)(\?.*)?$/,
        type: 'asset/resource',
        generator: { filename: 'img/[contenthash:8][ext][query]' }
      },
      // fonts
      {
        test: /\.(woff2?|ttf|otf)$/i,
        type: 'asset/resource',
        generator: { filename: 'fonts/[contenthash:8][ext][query]' }
      }
    ]
  }
}
