import { weatherService } from '@/services/WeatherService'
import { reactive } from 'vue'

export async function fetchWether(weather) {
  weather.loading = true

  return weatherService
    .get(weather)
    .then(({ data }) => {
      Object.assign(weather, {
        ...weather,
        data,
        hasError: false,
        updateTime: new Date().toLocaleTimeString('en-US')
      })
    })
    .catch(() => {
      weather.hasError = true
    })
    .finally(() => {
      weather.loading = false
    })
}

export function weatherContext(weather) {
  const reactiveWeather = reactive({
    loading: true,
    updateTime: null
  })

  Object.assign(reactiveWeather, weather)
  reactiveWeather.reload = async () => fetchWether(reactiveWeather)
  return reactiveWeather
}
