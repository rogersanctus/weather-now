import { camelcase } from '@/helpers/stringHelper'

const componentsContext = require.context('./', true, /\.vue$/, 'sync')

export function registerComponentsFor(vueApp) {
  componentsContext.keys().forEach((filename) => {
    const componentConfig = componentsContext(filename)
    const namePart = filename.split('/').pop()

    if (namePart !== undefined) {
      const componentName = camelcase(namePart.replace(/\.\w+$/, ''))

      vueApp.component(
        componentName,
        componentConfig.default || componentConfig
      )
    }
  })
}
