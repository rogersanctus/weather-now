export const Units = {
  temperature: { symbol: '°', description: 'Degrees Celsius' },
  humidity: { symbol: '%', description: 'Percentage' },
  pressure: { symbol: 'hPa', description: 'Hecto Pascal' }
}
