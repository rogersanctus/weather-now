export function capitalize(str) {
  return str.replace(/^(\w)/, (_match, p1) => p1.toUpperCase())
}

export function camelcase(str) {
  return capitalize(str).replace(/[_-\s](\w)/g, (_match, p1) =>
    p1.toUpperCase()
  )
}
