import { reactive, ref } from 'vue'

export function windowContext() {
  const isMobile = ref(false)
  let newIsMobile = false

  function checkIsMobile() {
    return window.innerWidth <= 360
  }

  function testWidth() {
    newIsMobile = false
    if (checkIsMobile()) {
      newIsMobile = true
    }

    // Avoid unnecessary triggering
    if (newIsMobile !== isMobile.value) {
      isMobile.value = newIsMobile
    }
  }

  isMobile.value = checkIsMobile()
  window.onresize = testWidth

  return reactive({ isMobile })
}
