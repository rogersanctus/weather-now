export const RefreshTime = 10 * 60 * 1000 // 10 minutes

class AutoRefresher {
  constructor(refreshTime = RefreshTime) {
    this.callbacks = []
    this.intervalId = setInterval(() => {
      this.callCb()
    }, refreshTime)

    this.callCb()
  }

  addCallback(callback) {
    if (typeof callback === 'function') {
      this.callbacks.push(callback)
    }
  }

  callCb() {
    this.callbacks.forEach((callback) => callback())
  }

  stop() {
    if (this.intervalId) {
      clearInterval(this.intervalId)
      this.intervalId = null
    }
  }
}

window.autoRefresher = new AutoRefresher()
