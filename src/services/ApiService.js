import axios from 'axios'

export class ApiService {
  constructor(baseUrl) {
    this.baseUrl = process.env.API_URL + '/' + baseUrl
  }

  get(params) {
    return axios.get(this.baseUrl, { params })
  }
}
