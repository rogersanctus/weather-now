import { ApiService } from './ApiService'

export class WeatherService extends ApiService {
  constructor() {
    super('data/2.5/weather')
  }

  makeCityParam(city) {
    return city.name + ',' + city.country
  }

  get(city) {
    return super.get({
      APPID: process.env.API_KEY,
      q: this.makeCityParam(city),
      units: 'metric'
    })
  }
}

export const weatherService = new WeatherService()
