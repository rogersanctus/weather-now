import { createApp } from 'vue'
import App from '@/App.vue'

import '@/index.scss'
import { registerComponentsFor } from './components/common'

import '@/services/AutoRefresher'

const app = createApp(App)
registerComponentsFor(app)

app.mount('#app')
